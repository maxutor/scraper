class CreateStats < ActiveRecord::Migration[6.0]
  def change
    create_table :stats do |t|
      t.belongs_to :post

      t.string :rank
      t.string :comment
      t.timestamps
    end
  end
end
