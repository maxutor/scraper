class AddUniqueConstraintTourls < ActiveRecord::Migration[6.0]
  def change
    add_index :sources, :url, unique: true
    add_index :posts, :url, unique: true
  end
end
