class RanksAndCommentsToInt < ActiveRecord::Migration[6.0]
  def change
    change_column :stats, :rank, :integer, using: 'rank::integer'
    change_column :stats, :comment, :integer, using: 'comment::integer'
  end
end
