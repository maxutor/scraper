module Api
  class PostsController < ApplicationController
    include Swagger::Blocks

    swagger_path '/api/posts' do
      operation :get do
        key :produces, [
          'application/json',
          'text/html',
        ]
        key :tags, [
          'post'
        ]
        parameter do
          key :name, :text
          key :in, :query
          key :description, 'text part of the post that the user is looking for'
          key :required, false
          key :type, :string
          key :format, :string
        end
        parameter do
          key :name, :sort
          key :in, :query
          key :description, 'parameter to sort by'
          key :required, false
          key :type, :string
          key :format, :string
        end
        parameter do
          key :name, :time
          key :in, :query
          key :description, 'the time the post was published, which the user is looking for'
          key :required, false
          key :type, :datetime
          key :format, :string
        end
        response 200 do
          key :description, 'post response'
          schema do
            key :'$ref', :Post
          end
        end
      end
    end

    def index
      @posts = Finders::PostFinder.new(request.query_parameters).call
      render json: PostSerializer.new(@posts, params: {stats: true, source: true})
    end
  end
end
