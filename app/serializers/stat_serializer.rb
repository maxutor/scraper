# == Schema Information
#
# Table name: stats
#
#  id         :bigint           not null, primary key
#  comment    :integer
#  rank       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  post_id    :bigint
#
# Indexes
#
#  index_stats_on_post_id  (post_id)
#
class StatSerializer
  include FastJsonapi::ObjectSerializer
  attributes :rank, :comment, :created_at
end
