# == Schema Information
#
# Table name: sources
#
#  id         :bigint           not null, primary key
#  url        :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_sources_on_url  (url) UNIQUE
#
class SourceSerializer
  include FastJsonapi::ObjectSerializer
  attributes :url
end
