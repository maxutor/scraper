# == Schema Information
#
# Table name: posts
#
#  id         :bigint           not null, primary key
#  title      :string
#  url        :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  source_id  :bigint
#
# Indexes
#
#  index_posts_on_source_id  (source_id)
#  index_posts_on_url        (url) UNIQUE
#
class PostSerializer
  include FastJsonapi::ObjectSerializer
  attributes :url, :title

  attribute :stats do |post, params|
    if params[:stats] == true
      StatSerializer.new(post.stats).serializable_hash
    end
  end
  attribute :source do |post, params|
    if params[:source] == true
      SourceSerializer.new(post.source).serializable_hash
    end
  end
end
