# == Schema Information
#
# Table name: posts
#
#  id         :bigint           not null, primary key
#  title      :string
#  url        :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  source_id  :bigint
#
# Indexes
#
#  index_posts_on_source_id  (source_id)
#  index_posts_on_url        (url) UNIQUE
#
class Post < ApplicationRecord
  include Swagger::Blocks
  include PgSearch::Model

  swagger_schema :Post do
    key :required, [:id, :source, :url, :title]
    property :id do
      key :type, :integer
      key :format, :int64
    end
    property :source do
      # key :type, :Source
      key :'$ref', :Pet
    end
    property :url do
      key :type, :string
    end
    property :title do
      key :type, :string
    end
    property :stats do
      key :type, :array
    end
  end

  pg_search_scope :search, against: :title, associated_against: {
    source: :url
  }

  belongs_to :source
  has_many :stats, dependent: :delete_all

  validates_presence_of :source, :url, :title

  paginates_per 10
end
