require 'sidekiq-scheduler'
require './lib/scrapers/hnews_scraper'

class ScrapWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    scraper = Scrapers::HnewsScraper.new
    data = scraper.posts_data

    posts = []

    data.each do |d|
      source = Source.find_or_create_by!(url: d[:source])
      post = source.posts.new(title: d[:title], url: d[:url])
      post.stats.build(rank: d[:rank], comment: d[:comment])
      posts << post
    end

    Post.import posts, recursive: true, on_duplicate_key_update: {
      conflict_target: [:url],
      columns: %i[title updated_at]
    }
  end
end
