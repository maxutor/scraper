module Finders
  class PostFinder

    def initialize(params = {})
      @posts = Post.all.includes(:stats, :source)
      @time = params[:time]
      @sort = params[:sort]
      @text = params[:text]
    end

    def call
      search_by_time if time
      search_by_text if text
      sort_posts

      # if @sort
      #   sort_posts
      # else
      #   @time = Time.now
      #   search_by_time
      # end

      current_state
    end

    private

    def search_by_text
      @posts.merge!(Post.search(text))
    end

    def search_by_time
      timestamp = Stat.where('created_at <= ?', time + 1.minute).order('created_at DESC').take.created_at
      @posts.merge!(Post.joins(:stats).where('stats.created_at = ?', timestamp))
    rescue
      raise(CustomErrors::UntrackedTimeError, @time)
    end

    def sort_posts
      # @posts.merge!(
      #   Post.joins(:stats).limit(10).order("stats.#{sort_key}")
      # )
      @posts.merge!((@posts.joins(:stats)).order("stats.#{sort_key}"))
    end

    def sliced_posts
      @posts.references(:stats).uniq.first(10)
    end

    def current_state
      sort_key == 'id DESC' ? sliced_posts.reverse : sliced_posts
    end

    # def sort_posts
    #   post_ids = Post.find_by_sql(%{
    #     WITH posts_scope AS
    #            (#{@posts.to_sql}),
    #          sorted_posts_scope AS
    #            (SELECT p.id,
    #                    s.id as s_id,
    #                    s.comment as s_comment,
    #                    s.rank as s_rank
    #             FROM posts_scope p
    #                    JOIN stats s ON s.post_id = p.id
    #             ORDER BY s.#{sort_key})
    #     SELECT distinct(sps.id)
    #     FROM sorted_posts_scope sps
    #     LIMIT 10 OFFSET 0
    #     }.squish
    #   ).pluck(:id)
    #
    #   @posts = Post.where(id: post_ids).order("stats.#{sort_key}")
    # end

    def time
      @time&.to_time
    rescue
      raise(CustomErrors::InvalidTimeError, @time)
    end

    def sort_key
      {
        rank: 'rank DESC',
        comment: 'comment DESC'
      }[@sort&.to_sym] || 'id DESC'
    end

    def text
      @text&.squish
    end
  end
end
