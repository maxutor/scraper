require 'httparty'
require 'nokogiri'

module Scrapers
  # https://news.ycombinator.com
  class HnewsScraper
    def initialize
      html = HTTParty.get('https://news.ycombinator.com')
      @parsed_page = Nokogiri::HTML(html)
      @data = fetch_data
    end

    def posts_data
      Array.new(10) do |index|
        {
          source: @data[:sources][index],
          title: @data[:titles][index],
          url: @data[:links][index],
          rank: @data[:ranks][index],
          comment: @data[:comments][index]
        }
      end
    end

    private

    def fetch_data
      {
        titles: fetch_text('td.title .storylink'),
        sources: fetch_text('.sitebit a'),
        links: fetch_href('a.storylink'),
        ranks: fetch_text('.subtext .score'),
        comments: fetch_text('.subtext a').select { |t| t.match('comment') }
      }
    end

    def fetch_text(path)
      @parsed_page.css(path).map(&:text)
    end

    def fetch_href(path)
      @parsed_page.css(path).map { |l| l[:href] }
    end
  end
end
