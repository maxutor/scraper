module CustomErrors
  class UntrackedTimeError < StandardError
    def initialize(time)
      super "No posts with a provided time #{time}"
    end
  end
end

