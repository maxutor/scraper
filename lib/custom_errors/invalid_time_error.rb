module CustomErrors
  class InvalidTimeError < StandardError
    def initialize(time)
      super "Provided time #{time} is invalid"
    end
  end
end
